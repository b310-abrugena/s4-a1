package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post; // import Post Class
import org.springframework.data.repository.CrudRepository; // Pre-build CrudRepository from Spring Framework
import org.springframework.stereotype.Repository;

@Repository
// By annotating it with @Repository, it becomes a Spring-managed repository component that handles data access operations for the "Post" entity.
// Post as model, in which its data type is an Object
public interface PostRepository extends CrudRepository <Post, Object>{
    // by extending CrudRepository, PostRepository has inherited its predefined methods for Creating, Retrieving, Updating and Deleting records
}
