package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController // annotation that handles HTTP request, and generating appropriate responses

// Basically, CORS allows us data sharing between different domains. To maintain integrity of the backend, we only allow some various websites to access our server.
// to enable Cross Origin Resource Sharing
// Example: When a client (browser) makes a request to a server, the server typically restricts access to its resources originating from other origin (different domain, protocol and port)
// There are cases where you might want to allow request from a different origin. This is where CORS comes into play or @CrossOrigin
@CrossOrigin // annotation that is used in order to become available in other origins

public class PostController {
    @Autowired // autowired annotation means
    PostService postService;

    // Create a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost (@RequestHeader (value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }
}
