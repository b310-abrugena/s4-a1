package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
/* The main goal of a RestController is to handle incoming HTTP requests and generate HTTP responses. */
/* Annotations are optional. They are primarily used to enhance behavior, configuration, or processing of code elements */
public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}

	// Request and Response
	@GetMapping("/hello")  // "/hello" as the endpoint
	public String hello(@RequestParam(value="name", defaultValue="World") String name){
		return String.format("Hello %s!", name); // hello function with return line as response
	}

	/* s01 Activity */
	@GetMapping("/hi")
	public String hi(@RequestParam(value="user", defaultValue="user") String user){
		return String.format("Hi %s!", user);
	}







}
